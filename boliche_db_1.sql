--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS;






--
-- Database creation
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1.pgdg90+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: barras_barra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.barras_barra (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.barras_barra OWNER TO postgres;

--
-- Name: barras_barra_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.barras_barra_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.barras_barra_id_seq OWNER TO postgres;

--
-- Name: barras_barra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.barras_barra_id_seq OWNED BY public.barras_barra.id;


--
-- Name: barras_bebida; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.barras_bebida (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.barras_bebida OWNER TO postgres;

--
-- Name: barras_bebida_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.barras_bebida_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.barras_bebida_id_seq OWNER TO postgres;

--
-- Name: barras_bebida_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.barras_bebida_id_seq OWNED BY public.barras_bebida.id;


--
-- Name: barras_envase; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.barras_envase (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.barras_envase OWNER TO postgres;

--
-- Name: barras_envase_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.barras_envase_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.barras_envase_id_seq OWNER TO postgres;

--
-- Name: barras_envase_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.barras_envase_id_seq OWNED BY public.barras_envase.id;


--
-- Name: barras_receta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.barras_receta (
    id integer NOT NULL,
    cantidad numeric(15,2) NOT NULL,
    bebida_id integer,
    trago_id integer
);


ALTER TABLE public.barras_receta OWNER TO postgres;

--
-- Name: barras_receta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.barras_receta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.barras_receta_id_seq OWNER TO postgres;

--
-- Name: barras_receta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.barras_receta_id_seq OWNED BY public.barras_receta.id;


--
-- Name: barras_trago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.barras_trago (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL,
    envase_id integer
);


ALTER TABLE public.barras_trago OWNER TO postgres;

--
-- Name: barras_trago_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.barras_trago_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.barras_trago_id_seq OWNER TO postgres;

--
-- Name: barras_trago_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.barras_trago_id_seq OWNED BY public.barras_trago.id;


--
-- Name: barras_venta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.barras_venta (
    id integer NOT NULL,
    fecha timestamp with time zone,
    cantidad integer NOT NULL,
    trago_id integer,
    barra_id integer
);


ALTER TABLE public.barras_venta OWNER TO postgres;

--
-- Name: barras_venta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.barras_venta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.barras_venta_id_seq OWNER TO postgres;

--
-- Name: barras_venta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.barras_venta_id_seq OWNED BY public.barras_venta.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: barras_barra id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_barra ALTER COLUMN id SET DEFAULT nextval('public.barras_barra_id_seq'::regclass);


--
-- Name: barras_bebida id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_bebida ALTER COLUMN id SET DEFAULT nextval('public.barras_bebida_id_seq'::regclass);


--
-- Name: barras_envase id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_envase ALTER COLUMN id SET DEFAULT nextval('public.barras_envase_id_seq'::regclass);


--
-- Name: barras_receta id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_receta ALTER COLUMN id SET DEFAULT nextval('public.barras_receta_id_seq'::regclass);


--
-- Name: barras_trago id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_trago ALTER COLUMN id SET DEFAULT nextval('public.barras_trago_id_seq'::regclass);


--
-- Name: barras_venta id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_venta ALTER COLUMN id SET DEFAULT nextval('public.barras_venta_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add barra	7	add_barra
26	Can change barra	7	change_barra
27	Can delete barra	7	delete_barra
28	Can view barra	7	view_barra
29	Can add bebida	8	add_bebida
30	Can change bebida	8	change_bebida
31	Can delete bebida	8	delete_bebida
32	Can view bebida	8	view_bebida
33	Can add trago	9	add_trago
34	Can change trago	9	change_trago
35	Can delete trago	9	delete_trago
36	Can view trago	9	view_trago
37	Can add receta	10	add_receta
38	Can change receta	10	change_receta
39	Can delete receta	10	delete_receta
40	Can view receta	10	view_receta
41	Can add envase	11	add_envase
42	Can change envase	11	change_envase
43	Can delete envase	11	delete_envase
44	Can view envase	11	view_envase
45	Can add venta	12	add_venta
46	Can change venta	12	change_venta
47	Can delete venta	12	delete_venta
48	Can view venta	12	view_venta
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$120000$2sC4NBhmhksC$tQc2aYdfAiVuzj+cRh+vjQDmnavTcsFa5ih4r5TXgPQ=	2019-04-08 21:58:44.911382+00	t	pindu				t	t	2019-04-08 21:56:38.582419+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: barras_barra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.barras_barra (id, nombre) FROM stdin;
1	uno
2	dos
\.


--
-- Data for Name: barras_bebida; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.barras_bebida (id, nombre) FROM stdin;
1	fernet
2	coca cola
3	cerveza lata 350
\.


--
-- Data for Name: barras_envase; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.barras_envase (id, nombre) FROM stdin;
1	vaso
2	jarra
3	lata chica
\.


--
-- Data for Name: barras_receta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.barras_receta (id, cantidad, bebida_id, trago_id) FROM stdin;
1	0.03	1	1
2	0.07	2	1
3	0.30	1	2
4	0.70	2	2
5	0.35	3	3
\.


--
-- Data for Name: barras_trago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.barras_trago (id, nombre, envase_id) FROM stdin;
1	fernet con coca	1
2	fernet con coca	2
3	cerveza	3
\.


--
-- Data for Name: barras_venta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.barras_venta (id, fecha, cantidad, trago_id, barra_id) FROM stdin;
2	2019-04-08 22:32:56+00	3	2	1
1	2019-04-08 22:32:01+00	2	1	2
3	2019-04-08 23:06:57+00	4	1	2
4	2019-04-09 09:02:10+00	4	3	1
5	2019-04-09 09:08:02+00	2	3	2
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2019-04-08 22:19:51.831917+00	1	vaso	1	[{"added": {}}]	11	1
2	2019-04-08 22:19:56.786962+00	2	jarra	1	[{"added": {}}]	11	1
3	2019-04-08 22:20:33.928291+00	1	fernet con coca	1	[{"added": {}}]	9	1
4	2019-04-08 22:20:45.674258+00	2	fernet con coca	1	[{"added": {}}]	9	1
5	2019-04-08 22:22:13.087054+00	1	fernet	1	[{"added": {}}]	8	1
6	2019-04-08 22:22:20.192936+00	2	coca cola	1	[{"added": {}}]	8	1
7	2019-04-08 22:23:02.758702+00	1	Receta object (1)	1	[{"added": {}}]	10	1
8	2019-04-08 22:24:55.693799+00	2	fernet con coca - vaso - coca cola - 0.07	1	[{"added": {}}]	10	1
9	2019-04-08 22:25:29.484662+00	3	fernet con coca - jarra - fernet - 0.3	1	[{"added": {}}]	10	1
10	2019-04-08 22:25:37.135702+00	4	fernet con coca - jarra - fernet - 0.7	1	[{"added": {}}]	10	1
11	2019-04-08 22:32:07.843239+00	1	Venta object (1)	1	[{"added": {}}]	12	1
12	2019-04-08 22:33:02.514524+00	2	fernet con coca - 3	1	[{"added": {}}]	12	1
13	2019-04-08 22:38:15.915134+00	1	uno	1	[{"added": {}}]	7	1
14	2019-04-08 22:38:18.962697+00	2	dos	1	[{"added": {}}]	7	1
15	2019-04-08 22:38:37.689249+00	2	fernet con coca - jarra - 3	2	[{"changed": {"fields": ["barra"]}}]	12	1
16	2019-04-08 22:38:42.708088+00	1	fernet con coca - vaso - 2	2	[{"changed": {"fields": ["barra"]}}]	12	1
17	2019-04-08 23:03:30.216326+00	4	fernet con coca - jarra - coca cola - 0.70	2	[{"changed": {"fields": ["bebida"]}}]	10	1
18	2019-04-08 23:07:08.920732+00	3	fernet con coca - vaso - 4 - dos	1	[{"added": {}}]	12	1
19	2019-04-09 09:00:08.005265+00	3	lata chica	1	[{"added": {}}]	11	1
20	2019-04-09 09:00:32.781947+00	3	cerveza	1	[{"added": {}}]	8	1
21	2019-04-09 09:01:15.300728+00	3	lata chica cerveza - lata chica	1	[{"added": {}}]	9	1
22	2019-04-09 09:01:26.502111+00	3	cerveza - lata chica	2	[{"changed": {"fields": ["nombre"]}}]	9	1
23	2019-04-09 09:01:49.757443+00	5	cerveza - lata chica - cerveza - 0.35	1	[{"added": {}}]	10	1
24	2019-04-09 09:02:17.108238+00	4	cerveza - lata chica - 4 - uno	1	[{"added": {}}]	12	1
25	2019-04-09 09:06:49.537889+00	3	cerveza lata 350	2	[{"changed": {"fields": ["nombre"]}}]	8	1
26	2019-04-09 09:07:42.527569+00	5	cerveza - lata chica - cerveza lata 350 - 0.35	2	[]	10	1
27	2019-04-09 09:08:11.138563+00	5	cerveza - lata chica - 2 - dos	1	[{"added": {}}]	12	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	barras	barra
8	barras	bebida
9	barras	trago
10	barras	receta
11	barras	envase
12	barras	venta
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2019-04-08 21:56:19.328733+00
2	auth	0001_initial	2019-04-08 21:56:19.535329+00
3	admin	0001_initial	2019-04-08 21:56:19.600394+00
4	admin	0002_logentry_remove_auto_add	2019-04-08 21:56:19.614538+00
5	admin	0003_logentry_add_action_flag_choices	2019-04-08 21:56:19.631595+00
6	contenttypes	0002_remove_content_type_name	2019-04-08 21:56:19.665534+00
7	auth	0002_alter_permission_name_max_length	2019-04-08 21:56:19.697371+00
8	auth	0003_alter_user_email_max_length	2019-04-08 21:56:19.720272+00
9	auth	0004_alter_user_username_opts	2019-04-08 21:56:19.739884+00
10	auth	0005_alter_user_last_login_null	2019-04-08 21:56:19.762413+00
11	auth	0006_require_contenttypes_0002	2019-04-08 21:56:19.766887+00
12	auth	0007_alter_validators_add_error_messages	2019-04-08 21:56:19.789095+00
13	auth	0008_alter_user_username_max_length	2019-04-08 21:56:19.821487+00
14	auth	0009_alter_user_last_name_max_length	2019-04-08 21:56:19.839776+00
15	sessions	0001_initial	2019-04-08 21:56:19.881023+00
16	barras	0001_initial	2019-04-08 22:07:51.555901+00
17	barras	0002_auto_20190408_1919	2019-04-08 22:19:25.726353+00
18	barras	0003_venta	2019-04-08 22:31:54.545223+00
19	barras	0004_venta_barra	2019-04-08 22:38:29.401721+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
qlstlb47l2y1x59pkesh2656ha6o6szo	NTJlZDU2ZDE5MGQ0MjIxZDcxYzZkYWZiM2EzNTM5ZmE3Y2M2NGFjZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2NzIxMGY2Yzg1MGUwYzIyNDQzZDVlMDA3ODYyNTE3YTg2MTQwM2FkIn0=	2019-04-22 21:58:44.914717+00
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 48, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: barras_barra_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.barras_barra_id_seq', 2, true);


--
-- Name: barras_bebida_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.barras_bebida_id_seq', 3, true);


--
-- Name: barras_envase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.barras_envase_id_seq', 3, true);


--
-- Name: barras_receta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.barras_receta_id_seq', 5, true);


--
-- Name: barras_trago_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.barras_trago_id_seq', 3, true);


--
-- Name: barras_venta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.barras_venta_id_seq', 5, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 27, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 12, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 19, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: barras_barra barras_barra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_barra
    ADD CONSTRAINT barras_barra_pkey PRIMARY KEY (id);


--
-- Name: barras_bebida barras_bebida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_bebida
    ADD CONSTRAINT barras_bebida_pkey PRIMARY KEY (id);


--
-- Name: barras_envase barras_envase_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_envase
    ADD CONSTRAINT barras_envase_pkey PRIMARY KEY (id);


--
-- Name: barras_receta barras_receta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_receta
    ADD CONSTRAINT barras_receta_pkey PRIMARY KEY (id);


--
-- Name: barras_trago barras_trago_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_trago
    ADD CONSTRAINT barras_trago_pkey PRIMARY KEY (id);


--
-- Name: barras_venta barras_venta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_venta
    ADD CONSTRAINT barras_venta_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: barras_receta_bebida_id_39a60a0f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX barras_receta_bebida_id_39a60a0f ON public.barras_receta USING btree (bebida_id);


--
-- Name: barras_receta_trago_id_4a12d4b9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX barras_receta_trago_id_4a12d4b9 ON public.barras_receta USING btree (trago_id);


--
-- Name: barras_trago_envase_id_5fb445ba; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX barras_trago_envase_id_5fb445ba ON public.barras_trago USING btree (envase_id);


--
-- Name: barras_venta_barra_id_6fdf5a3c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX barras_venta_barra_id_6fdf5a3c ON public.barras_venta USING btree (barra_id);


--
-- Name: barras_venta_trago_id_79494c85; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX barras_venta_trago_id_79494c85 ON public.barras_venta USING btree (trago_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: barras_receta barras_receta_bebida_id_39a60a0f_fk_barras_bebida_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_receta
    ADD CONSTRAINT barras_receta_bebida_id_39a60a0f_fk_barras_bebida_id FOREIGN KEY (bebida_id) REFERENCES public.barras_bebida(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: barras_receta barras_receta_trago_id_4a12d4b9_fk_barras_trago_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_receta
    ADD CONSTRAINT barras_receta_trago_id_4a12d4b9_fk_barras_trago_id FOREIGN KEY (trago_id) REFERENCES public.barras_trago(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: barras_trago barras_trago_envase_id_5fb445ba_fk_barras_envase_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_trago
    ADD CONSTRAINT barras_trago_envase_id_5fb445ba_fk_barras_envase_id FOREIGN KEY (envase_id) REFERENCES public.barras_envase(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: barras_venta barras_venta_barra_id_6fdf5a3c_fk_barras_barra_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_venta
    ADD CONSTRAINT barras_venta_barra_id_6fdf5a3c_fk_barras_barra_id FOREIGN KEY (barra_id) REFERENCES public.barras_barra(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: barras_venta barras_venta_trago_id_79494c85_fk_barras_trago_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barras_venta
    ADD CONSTRAINT barras_venta_trago_id_79494c85_fk_barras_trago_id FOREIGN KEY (trago_id) REFERENCES public.barras_trago(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1.pgdg90+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

