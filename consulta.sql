/*
postgres=# \dt
                   List of relations
 Schema |            Name            | Type  |  Owner   
--------+----------------------------+-------+----------
 public | auth_group                 | table | postgres
 public | auth_group_permissions     | table | postgres
 public | auth_permission            | table | postgres
 public | auth_user                  | table | postgres
 public | auth_user_groups           | table | postgres
 public | auth_user_user_permissions | table | postgres
 public | barras_barra               | table | postgres
 public | barras_bebida              | table | postgres
 public | barras_envase              | table | postgres
 public | barras_receta              | table | postgres
 public | barras_trago               | table | postgres
 public | barras_venta               | table | postgres
 public | django_admin_log           | table | postgres
 public | django_content_type        | table | postgres
 public | django_migrations          | table | postgres
 public | django_session             | table | postgres
(16 rows)    
*/
/*
SELECT 
    barras_venta.fecha as fecha,
    barras_venta.cantidad as cantidad,
    barras_trago.nombre as trago, 
    barras_barra.nombre as barra, 
    barras_envase.nombre as envase

FROM 
    barras_venta 
    inner join barras_barra on (barras_barra.id=barras_venta.barra_id) 
    inner join barras_trago on (barras_trago.id=barras_venta.trago_id)
    inner join barras_envase on (barras_trago.envase_id = barras_envase.id)
    ;
*/
--RESULTADO
/*
         fecha          | cantidad |      trago      | barra | envase 
------------------------+----------+-----------------+-------+--------
 2019-04-08 22:32:56+00 |        3 | fernet con coca | uno   | jarra
 2019-04-08 22:32:01+00 |        2 | fernet con coca | dos   | vaso
 2019-04-08 23:06:57+00 |        4 | fernet con coca | dos   | vaso
(3 rows)
*/
/*
SELECT 
    barras_venta.fecha as fecha,
    barras_venta.cantidad as cantidad,
    barras_trago.nombre as trago, 
    barras_barra.nombre as barra, 
    barras_envase.nombre as envase,
    barras_receta.cantidad as cantidad_de_bebida_en_litros,
    barras_bebida.nombre as nombre_bebida

FROM 
    barras_venta 
    inner join barras_barra on (barras_barra.id=barras_venta.barra_id) 
    inner join barras_trago on (barras_trago.id=barras_venta.trago_id)
    inner join barras_envase on (barras_trago.envase_id = barras_envase.id)
    inner join barras_receta on (barras_receta.trago_id = barras_trago.id)
    inner join barras_bebida on (barras_receta.bebida_id = barras_bebida.id)
    ;
*/
--RESULTADO
/*
         fecha          | cantidad |      trago      | barra | envase | cantidad_de_bebida_en_litros 
------------------------+----------+-----------------+-------+--------+------------------------------
 2019-04-08 22:32:56+00 |        3 | fernet con coca | uno   | jarra  |                         0.70
 2019-04-08 22:32:56+00 |        3 | fernet con coca | uno   | jarra  |                         0.30
 2019-04-08 22:32:01+00 |        2 | fernet con coca | dos   | vaso   |                         0.07
 2019-04-08 22:32:01+00 |        2 | fernet con coca | dos   | vaso   |                         0.03
 2019-04-08 23:06:57+00 |        4 | fernet con coca | dos   | vaso   |                         0.07
 2019-04-08 23:06:57+00 |        4 | fernet con coca | dos   | vaso   |                         0.03
(6 rows)
*/
/*
SELECT 
    SUM(barras_receta.cantidad*barras_venta.cantidad) as litros_necesarios,
    barras_bebida.nombre as nombre_bebida

FROM 
    barras_venta 
    inner join barras_barra on (barras_barra.id=barras_venta.barra_id) 
    inner join barras_trago on (barras_trago.id=barras_venta.trago_id)
    inner join barras_envase on (barras_trago.envase_id = barras_envase.id) 
    inner join barras_receta on (barras_receta.trago_id = barras_trago.id)
    inner join barras_bebida on (barras_receta.bebida_id = barras_bebida.id)
GROUP BY
    2
    ;
    */
--RESULTADO
/*
 litros_necesarios | nombre_bebida 
-------------------+---------------
              2.52 | coca cola
              1.08 | fernet
(2 rows)
*/

SELECT 
    SUM(barras_receta.cantidad*barras_venta.cantidad) as litros_necesarios,
    barras_bebida.nombre as nombre_bebida

FROM 
    barras_venta 
    inner join barras_barra on (barras_barra.id=barras_venta.barra_id) 
    inner join barras_trago on (barras_trago.id=barras_venta.trago_id)
    inner join barras_envase on (barras_trago.envase_id = barras_envase.id) 
    inner join barras_receta on (barras_receta.trago_id = barras_trago.id)
    inner join barras_bebida on (barras_receta.bebida_id = barras_bebida.id)
GROUP BY
    2
HAVING 
    SUM(barras_receta.cantidad*barras_venta.cantidad) > 2
    ;

--Creación de vistas
CREATE OR REPLACE VIEW bebida_consumida AS 
    SELECT 
        SUM(barras_receta.cantidad*barras_venta.cantidad) as litros_necesarios,
        barras_bebida.nombre as nombre_bebida

    FROM 
        barras_venta 
        inner join barras_barra on (barras_barra.id=barras_venta.barra_id) 
        inner join barras_trago on (barras_trago.id=barras_venta.trago_id)
        inner join barras_envase on (barras_trago.envase_id = barras_envase.id) 
        inner join barras_receta on (barras_receta.trago_id = barras_trago.id)
        inner join barras_bebida on (barras_receta.bebida_id = barras_bebida.id)
    GROUP BY
        2
;

SELECT * 
FROM 
    bebida_consumida;

SELECT * 
FROM 
    bebida_consumida
WHERE 
    litros_necesarios > 2;

--Consultas Anidadas

SELECT *
FROM
    (SELECT 
        SUM(barras_receta.cantidad*barras_venta.cantidad) as litros_necesarios,
        barras_bebida.nombre as nombre_bebida

    FROM 
        barras_venta 
        inner join barras_barra on (barras_barra.id=barras_venta.barra_id) 
        inner join barras_trago on (barras_trago.id=barras_venta.trago_id)
        inner join barras_envase on (barras_trago.envase_id = barras_envase.id) 
        inner join barras_receta on (barras_receta.trago_id = barras_trago.id)
        inner join barras_bebida on (barras_receta.bebida_id = barras_bebida.id)
    GROUP BY
        2) v 
WHERE
    litros_necesarios > 2;

--Consultas anidadas

SELECT *
FROM
    (SELECT 
        SUM(barras_receta.cantidad*barras_venta.cantidad) as litros_necesarios,
        barras_bebida.nombre as nombre_bebida

    FROM 
        barras_venta 
        inner join barras_barra on (barras_barra.id=barras_venta.barra_id) 
        inner join barras_trago on (barras_trago.id=barras_venta.trago_id)
        inner join barras_envase on (barras_trago.envase_id = barras_envase.id) 
        inner join barras_receta on (barras_receta.trago_id = barras_trago.id)
        inner join barras_bebida on (barras_receta.bebida_id = barras_bebida.id)
    WHERE
        --barras_bebida.nombre in ('coca cola','fernet')
        barras_bebida.nombre in (SELECT nombre
                                    FROM  
                                        barras_bebida
                                    WHERE
                                        nombre = 'coca cola' OR nombre = 'fernet'  
                                    )
    GROUP BY
        2) v 
WHERE
    litros_necesarios > 1;

SELECT *
FROM
    (SELECT 
        SUM(barras_receta.cantidad*barras_venta.cantidad) as litros_necesarios,
        barras_bebida.nombre as nombre_bebida

    FROM 
        barras_venta 
        inner join barras_barra on (barras_barra.id=barras_venta.barra_id) 
        inner join barras_trago on (barras_trago.id=barras_venta.trago_id)
        inner join barras_envase on (barras_trago.envase_id = barras_envase.id) 
        inner join barras_receta on (barras_receta.trago_id = barras_trago.id)
        inner join barras_bebida on (barras_receta.bebida_id = barras_bebida.id)
    WHERE
        --barras_bebida.nombre in ('coca cola','fernet')
        barras_bebida.nombre in (SELECT nombre
                                    FROM  
                                        barras_bebida
                                    WHERE
                                        nombre = 'coca cola' OR nombre = 'fernet'  
                                    )
    GROUP BY
        2) v 
WHERE
    litros_necesarios > 1;


--usando la vista
SELECT
    *
FROM 
    bebida_consumida
WHERE 
    litros_necesarios > 1
    AND nombre_bebida in (SELECT nombre
                                    FROM  
                                        barras_bebida
                                    WHERE
                                        nombre = 'coca cola' OR nombre = 'fernet'  
                                    )
