#!/bin/sh
PROJECT=${PWD##*/}
docker exec -it ${PROJECT}_api_1 python manage.py makemigrations
docker stop ${PROJECT}_api_1
docker exec -it ${PROJECT}_db_1 dropdb -U postgres postgres
docker exec -it ${PROJECT}_db_1 createdb -U postgres postgres
cat ./${PROJECT}_db_1.sql | docker exec -i ${PROJECT}_db_1 psql -U postgres
docker start ${PROJECT}_api_1
docker exec -it ${PROJECT}_api_1 python manage.py migrate