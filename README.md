# boliche

Trabajo Práctico de modelados de comprobantes en un boliche.
Para correr el proyecto, hay que tener instalado docker y docker-compose
se debe ejecutar el siguiente comando en la carpeta del proyecto
```sh
docker-compose --project-name=${PWD##*/} up --build --force-recreate -d api
```

Una vez que el proyecto esta corriendo se debe ejecutar el siguiente comando para crear las tablas necesarias
```sh
docker exec -it ${PWD##*/}_api_1 python manage.py migrate
```

Una vez finalizada la creación debemos crear un usuario. Ejecutamos el siguiente comando para crear un superusuario
```sh
docker exec -it ${PWD##*/}_api_1 python manage.py createsuperuser
```

Antes de finalizar reiniciamos el contenedor, volvemos a ejecutar el siguiente comando
```sh
docker-compose --project-name=${PWD##*/} up --build --force-recreate -d api
```

y por ultimo verificamos que todo este funcionando
```sh
docker logs -f ${PWD##*/}_api_1
```

Ingresamos con el usuario que recién creamos a:

http://localhost:8007/admin/

Para restaurar el backup debemos ejecutar el siguiente comando
```sh
. restore_db.sh
```
* Para ejecutar el script, copio el archivo dentro del contenedor del postgresql
```sh
docker cp consulta.sql boliche_db_1:. 
```
* Para ejecutar el script utilizo el siguiente comando
```sh
docker exec -i boliche_db_1 psql -U postgres -f consulta.sql
```
* Se puede ejecutar el script sin copiarlo dentro del contenedor de la siguiente manera
```sh
cat consulta.sql | docker exec -i boliche_db_1 psql -U postgres
```

