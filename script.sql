--\d barras_bebida
/*
public | barras_barra               | table | postgres
 public | barras_bebida              | table | postgres
 public | barras_envase              | table | postgres
 public | barras_receta              | table | postgres
 public | barras_trago               | table | postgres
 public | barras_venta               | table | postgres
 */

SELECT
    nombre as "Nombre Bebida"
FROM 
    barras_bebida
;

--\d barras_trago

SELECT
    barras_venta.fecha,
    barras_venta.cantidad,
    upper(barras_trago.nombre) || ', ' || upper(barras_envase.nombre) as "Nombre del Trago"
FROM    
    barras_venta
    inner join barras_trago on (barras_venta.trago_id = barras_trago.id)
    inner join barras_envase on (barras_trago.envase_id = barras_envase.id)
WHERE   
    upper(barras_trago.nombre) LIKE upper('%coca%')
ORDER BY
    2 DESC, 3
;