from django.contrib import admin
from django.apps import apps
from .models import *

@admin.register(Barra)
class TurnosAtendidosdmin(admin.ModelAdmin):
    date_hierarchy = 'fecha'
    list_per_page = 50
    list_display = ('nombre','cantidad',)
    list_filter = ['nombre'
    ]
    search_fields = ['nombre',  ]


'''
# Register your models here.
app = apps.get_app_config('barras')
for model in app.get_models():
    try:
        admin.site.register(model)
    except AlreadyRegistered:
        pass
'''