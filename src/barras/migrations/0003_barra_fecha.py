# Generated by Django 2.2 on 2019-05-17 22:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('barras', '0002_barra_cantidad'),
    ]

    operations = [
        migrations.AddField(
            model_name='barra',
            name='fecha',
            field=models.DateTimeField(help_text='Fecha y hora de la venta', null=True, verbose_name='Fecha de venta'),
        ),
    ]
