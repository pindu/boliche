from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
import datetime

# Create your models here.
class NombreAbstract(models.Model):
    nombre = models.CharField(
        _('Nombre'),
        max_length=200,
        help_text=_('Nombre'),
        null = False,
        blank = False
    )

    def __str__(self):
        return '{}'.format(self.nombre)

    class Meta:
        abstract = True
        ordering = ['nombre']

class Barra(NombreAbstract):
    fecha = models.DateTimeField(
        _('Fecha de venta'),
        help_text=_('Fecha y hora de la venta'),
        null=True
    )
    cantidad = models.DecimalField(
        _('cantidad'),
        max_digits=15,
        decimal_places=2,
        help_text=_('cantidad en litros de la composicion'),
        null=True
    )
    class Meta:
        verbose_name = _('barra')
        verbose_name_plural = _('barras')

class Bebida(NombreAbstract):
    class Meta:
        verbose_name = _('bebida')
        verbose_name_plural = _('bebidas')

class Envase(NombreAbstract):
    class Meta:
        verbose_name = _('envase')
        verbose_name_plural = _('envases')

class Trago(NombreAbstract):
    envase = models.ForeignKey(Envase,
        related_name = 'trago',
        verbose_name = _('envase'),
        on_delete=models.PROTECT,
        help_text=_('envase en el que se presenta el trago'),
        null=True
    )
    def __str__(self):
        return '{} - {}'.format(self.nombre,self.envase.nombre)
    class Meta:
        verbose_name = _('trago')
        verbose_name_plural = _('tragos')

class Receta(models.Model):
    trago = models.ForeignKey(Trago,
        related_name = 'receta',
        verbose_name = _('trago'),
        on_delete=models.PROTECT,
        help_text=_('trago'),
        null=True
    )
    bebida = models.ForeignKey(Bebida,
        related_name = 'receta',
        verbose_name = _('bebida'),
        on_delete=models.PROTECT,
        help_text=_('bebida'),
        null=True
    )
    cantidad = models.DecimalField(
        _('cantidad'),
        max_digits=15,
        decimal_places=2,
        help_text=_('cantidad en litros de la composicion'),
    )
    def __str__(self):
        return '{} - {} - {} - {}'.format(self.trago.nombre,self.trago.envase.nombre, self.bebida.nombre, self.cantidad)

class Venta(models.Model):
    fecha = models.DateTimeField(
        _('Fecha de venta'),
        help_text=_('Fecha y hora de la venta'),
        null=True
    )
    cantidad = models.IntegerField(
        _('cantidad'),
        help_text=_('cantidad de tragos'),
    )
    trago = models.ForeignKey(Trago,
        related_name = 'venta',
        verbose_name = _('trago'),
        on_delete=models.PROTECT,
        help_text=_('trago'),
        null=True
    )
    barra = models.ForeignKey(Barra,
        related_name = 'venta',
        verbose_name = _('barra'),
        on_delete=models.PROTECT,
        help_text=_('barra'),
        null=True
    )
    def __str__(self):
        return '{} - {} - {} - {}'.format(self.trago.nombre, self.trago.envase.nombre, self.cantidad, self.barra.nombre,)
